package com.kgc.cn.consumer.utils.wx;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Ding on 2019/12/2.
 */
@Component
@ConfigurationProperties(value = "wx")
public class WxUtils {
    private String codeUrl;
    private String appId;
    private String redirectUri;
    private String scope;
    private String AccessTokenUrl;
    private String secret;
    private String UserInfoUrl;


    public String getCodeUrl() throws UnsupportedEncodingException {
        StringBuffer sbf = new StringBuffer(codeUrl);
        sbf.append("appid=").append(appId)
                .append("&redirect_uri=").append(URLEncoder.encode(redirectUri, "UTF-8"))
                .append("&response_type=").append("codePic")
                .append("&scope=").append(scope)
                .append("&state=").append("STATE")
                .append("#wechat_redirect");
        return sbf.toString();
    }

    public String getAccrssTokenUrl(String code) {
        StringBuffer sbf = new StringBuffer(AccessTokenUrl);
        sbf.append("appid=").append(appId)
                .append("&secret=").append(secret)
                .append("&code=").append(code)
                .append("&grant_type=").append("authorization_code");
        return sbf.toString();
    }

    public String getUserInfo(String accessToken, String oppenId) {
        StringBuffer sbf = new StringBuffer(UserInfoUrl);
        sbf.append("access_token=").append(accessToken)
                .append("&openid=").append(oppenId)
                .append("&lang=").append("zh_CN");
        return sbf.toString();
    }

    public void setCodeUrl(String codeUrl) {
        this.codeUrl = codeUrl;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getAccessTokenUrl() {
        return AccessTokenUrl;
    }

    public void setAccessTokenUrl(String accessTokenUrl) {
        AccessTokenUrl = accessTokenUrl;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getUserInfoUrl() {
        return UserInfoUrl;
    }

    public void setUserInfoUrl(String userInfoUrl) {
        UserInfoUrl = userInfoUrl;
    }
}
