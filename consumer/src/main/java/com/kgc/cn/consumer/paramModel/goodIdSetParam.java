package com.kgc.cn.consumer.paramModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

/**
 * @auther zhouxinyu
 * @data 2019/12/23
 */
@Data
@ApiModel(value = "id存放")
@AllArgsConstructor
@NoArgsConstructor
public class goodIdSetParam implements Serializable {
    @ApiModelProperty(value = "地址id", example = "1")
    int shippingId;
    @ApiModelProperty(value = "商品id集合")
    private Set<String> goodIdSet;
}
