package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.Shopcar;
import com.kgc.cn.common.model.dto.ShopcarExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShopcarMapper {
    long countByExample(ShopcarExample example);

    int deleteByExample(ShopcarExample example);

    int deleteByPrimaryKey(String phone);

    int insert(Shopcar record);

    int insertSelective(Shopcar record);

    List<Shopcar> selectByExample(ShopcarExample example);

    Shopcar selectByPrimaryKey(String phone);

    int updateByExampleSelective(@Param("record") Shopcar record, @Param("example") ShopcarExample example);

    int updateByExample(@Param("record") Shopcar record, @Param("example") ShopcarExample example);

    int updateByPrimaryKeySelective(Shopcar record);

    int updateByPrimaryKey(Shopcar record);

    // 通过手机号和商品id删除购物车信息
    int deleteByPhoneAndGoodId(@Param("goodId") String goodId, @Param("phone") String phone);

    // 通过手机查询购物车
    List<Shopcar> queryShopcarByPhone(String phone);

    // 更新手机号下，该商品id对应的商品数量
    int updateShopCarByPhone(@Param("shopcar") Shopcar shopcar);
}