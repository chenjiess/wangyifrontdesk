package com.kgc.cn.common.service;

import com.kgc.cn.common.model.dto.Goods;

import java.util.List;

public interface ProductsCommonService {

    //商品列表要按类型来展示商品列表（从后台中获取商品列表）
    List<Goods> ProductList(int typeId, int current, int size) throws Exception;

    //查询该类型商品总数量
    int queryCount(int typeId);

    //商品详情（要体现 名字 原价 实付价格 图片 描述）
    Goods ProductsDetails(String goodId);

    //商品：折扣
    int DiscountParam(String goodId) throws Exception;

    // 更新商品库存
    int ProductNum(String goodId);

    // 获取商品库存
    int ProductsNum(String goodId);
}
