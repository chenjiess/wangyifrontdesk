package com.kgc.cn.common.enums;

/**
 * Created by boot on 2019/12/24
 */
public enum ShopcarEnum {

    PHONENUM_ERROR(200, "未绑定手机号！"),
    SHOPNUM_ERROR(201, "输入数量错误！"),
    SHOPCAR_NULL_ERROR(000, "购物车空空滴~~");

    int code;
    String msg;

    ShopcarEnum() {
    }

    ShopcarEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
