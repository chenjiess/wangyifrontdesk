package com.kgc.cn.common.service;

import com.kgc.cn.common.model.dto.Goods;
import com.kgc.cn.common.model.dto.Promotion;
import com.kgc.cn.common.model.dto.Shopcar;

import java.util.List;

/**
 * Created by boot on 2019/12/24
 */
public interface ShopcarCommonService {
    // 通过手机号添加购物车
    int insertByPhone(Shopcar shopcar);

    // 通过手机号和商品id删除购物车信息
    int deleteByPhoneAndGoodId(String phone, String goodId);

    // 查询改手机号下，对应商品id的购物车对象
    Shopcar queryShopCarByGoodId(String phone, String goodId);

    //更新手机号下，该商品id对应的商品数量
    int updateShopCarByPhone(Shopcar shopcar);

    // 通过商品id查询商品信息
    Goods queryGoodByGoodId(String goodId);

    // 通过goodId查询折扣信息
    Promotion queryPromotionByGoodId(String goodId);

    // 通过手机号查询购物车
    List<Shopcar> queryShopcarByPhone(String phone);
}
