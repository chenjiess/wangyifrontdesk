package com.kgc.cn.common.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class Goodorder implements Serializable {
    private static final long serialVersionUID = -3114256632141443404L;
    //订单id
    private String orderId;
    //手机号
    private String phone;
    //商品id
    private String goodId;
    //购买金额
    private String shopMoney;
    //下单时间
    private String orderTime;
    //订单状态
    private Integer typeId;
    //收货地址id
    private Integer shippingId;
}