package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 4786030592545462084L;
    //普通用户id
    private String userId;
    //普通用户名
    private String userName;
    //手机号
    private String phone;
    //密码
    private String password;
    //收货地址
    private String address;
    //年龄
    private Integer age;
    //性别
    private Integer sex;
    //头像地址
    private String headImgUrl;
}